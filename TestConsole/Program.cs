﻿using gRPC.Contract;
using gRPC.Secure.Client.Test.Services;
using grRPC.Secure.Client.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;

namespace TestConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
             .Enrich.FromLogContext()
             .WriteTo.Console()
             .CreateLogger();

            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder()
                .UseSerilog()
                .ConfigureServices(ConfigureService);

        private static void ConfigureService(HostBuilderContext hostContext, IServiceCollection services)
        {
            services.AddSecureGRPCClient<Greeter.GreeterClient>(new SecureGrpcClientOptions()
            {
                Address = "https://localhost:5001",
                CertificateSubjectName = "Ruan Schoombee"
            });
            services.AddHostedService<SecureGrpcClientFactoryExample>();
            services.AddHostedService<SecureGrpcClientFactoryDIExample>();
        }
    }
}
