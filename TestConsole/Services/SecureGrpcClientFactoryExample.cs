﻿using gRPC.Contract;
using gRPC.Secure.Client.Extensions;
using grRPC.Secure.Client.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace gRPC.Secure.Client.Test.Services
{
    public class SecureGrpcClientFactoryExample : IHostedService
    {
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger<SecureGrpcClientFactoryExample> logger;

        public SecureGrpcClientFactoryExample(ILoggerFactory loggerFactory)
        {
            this.loggerFactory = loggerFactory;
            logger = loggerFactory.CreateLogger<SecureGrpcClientFactoryExample>();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            for (var i = 0; i < 5; i++)
            {
                var wacth = new Stopwatch();
                wacth.Start();

                var client = SecureGrpcClientFactory.Create<Greeter.GreeterClient>(new SecureGrpcClientOptions()
                {
                    Address = "https://localhost:5001",
                    CertificateSubjectName = "Ruan Schoombee",
                    LoggerFactory = loggerFactory
                });

                var response = client.SayHello(new HelloRequest()
                {
                    Name = "Ruan Schoombee"
                });

                wacth.Stop();
                logger.LogInformation($"Response Received | Payload={response} TimeEplapsed={wacth.ElapsedMilliseconds}ms");
            }

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
