﻿using gRPC.Contract;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using static gRPC.Contract.Greeter;

namespace gRPC.Secure.Client.Test.Services
{
    public class SecureGrpcClientFactoryDIExample : IHostedService
    {
        private readonly GreeterClient greeterClient;
        private ILogger<SecureGrpcClientFactoryDIExample> logger;

        public SecureGrpcClientFactoryDIExample(GreeterClient greeterClient, ILogger<SecureGrpcClientFactoryDIExample> logger)
        {
            this.greeterClient = greeterClient;
            this.logger = logger;
        }


        public Task StartAsync(CancellationToken cancellationToken)
        {
            for(var i = 0; i < 5; i++)
            {
                var wacth = new Stopwatch();

                wacth.Start();

                var response = greeterClient.SayHello(new HelloRequest()
                {
                    Name = "Ruan Schoombee"
                });

                wacth.Stop();
                logger.LogInformation($"Response Received | Payload={response} TimeEplapsed={wacth.ElapsedMilliseconds}ms");
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
