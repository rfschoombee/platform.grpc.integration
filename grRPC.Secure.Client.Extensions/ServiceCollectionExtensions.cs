﻿using gRPC.Secure.Client.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;

namespace grRPC.Secure.Client.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSecureGRPCClient<TClient>(this IServiceCollection services, SecureGrpcClientOptions options) where TClient : class
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services
                .AddGrpcClient<TClient>(o => o.Address = new Uri(options.Address))
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    var handler = new HttpClientHandler();
                    handler.ClientCertificates.AddCertificate(options.CertificateSubjectName);

                    return handler;
                });

            return services;
        }

        
    }
}
