﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace gRPC.Secure.Client.Extensions
{
    public static class InstanceFactory<TClient> where TClient : class
    {
        public delegate object ConstructorDelegate(params object[] args);
        private static ConstructorDelegate CreateConstructor(params Type[] parameters)
        {
            var constructorInfo = typeof(TClient).GetConstructor(parameters);

            var paramExpr = Expression.Parameter(typeof(object[]));

            var constructorParameters = parameters.Select((paramType, index) =>
                Expression.Convert(Expression.ArrayAccess(paramExpr, Expression.Constant(index)), paramType))
                .ToArray();

            var body = Expression.New(constructorInfo, constructorParameters);

            var constructor = Expression.Lambda<ConstructorDelegate>(body, paramExpr);
            return constructor.Compile();
        }

        public static TClient Create(params object[] args) =>
            CreateConstructor(args.Select(o => o.GetType()).ToArray())(args)
            as TClient;
    }
}
