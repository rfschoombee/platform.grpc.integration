﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace gRPC.Secure.Client.Extensions
{
    public static class X509CertificateCollectionExtensions
    {
        public static void AddCertificate(this X509CertificateCollection certificateCollection, string certificateSubjectName)
        {
            X509Certificate2 certificate = null;

            using (var certificateStore = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                certificateStore.Open(OpenFlags.ReadOnly);

                var certificates = certificateStore
                    .Certificates
                    .Find(X509FindType.FindBySubjectName, certificateSubjectName, false);

                certificate = certificates[0]
                    ?? throw new InvalidOperationException($"Certificate with CN = {certificateSubjectName} not found in StoreLocation = {StoreLocation.CurrentUser}");
            }

            certificateCollection.Add(certificate);
        }
    }
}
