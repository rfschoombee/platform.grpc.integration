﻿using Grpc.Net.Client;
using grRPC.Secure.Client.Extensions;
using System;
using System.Net.Http;

namespace gRPC.Secure.Client.Extensions
{
    public static class SecureGrpcClientFactory
    {
        public static TClient Create<TClient>(SecureGrpcClientOptions options) where TClient : class
        {
            var handler = new HttpClientHandler();

            handler.ClientCertificates
                .AddCertificate(options.CertificateSubjectName);

            var channel = GrpcChannel.ForAddress(options.Address, new GrpcChannelOptions
            {
                HttpHandler = handler,
                LoggerFactory = options.LoggerFactory
            });

            return Activator.CreateInstance(typeof(TClient), new object[] { channel }) as TClient;

            // Compiles an expression intitialization takes longer but subsequent calls has better performance.
            // Uncomment the line below to use InstanceFactory<TClient>.
            //return InstanceFactory<TClient>.Create(channel);
        }
    }
}
