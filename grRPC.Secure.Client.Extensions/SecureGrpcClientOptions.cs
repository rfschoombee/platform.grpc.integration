﻿using Microsoft.Extensions.Logging;

namespace grRPC.Secure.Client.Extensions
{
    public class SecureGrpcClientOptions
    {
        public string Address { get; set; }
        public string CertificateSubjectName { get; set; }
        public ILoggerFactory LoggerFactory { get; set; }

        public SecureGrpcClientOptions(string address, string certificateSubjectName)
        {
            Address = address;
            CertificateSubjectName = certificateSubjectName;
        }

        public SecureGrpcClientOptions()
        {

        }
    }
}